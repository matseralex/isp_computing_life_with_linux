Course consists from 6 topics. 

Each topic consists from initial lecture part and practical seminar with students.

Students should bring laptops and either install any kind of linux system or
 setup virtual machine there.

The course is devoted to series of frequently asked questions from people who
 start their scientific computing life with Linux. We'll give a masterclass
 for a work within the ssh-session, standard terminal commands and their
 combinations, tips on organization of the simplest possible bash-scripts
 (loops, background calculations, IO-redirections, etc.).

We'll explain and demonstrate the gentleman's set for software compilation 
from source (user configuration files, environment variables, Makefiles basics,
 compiler options and optimization flags, linking external libraries and
 connection of these concepts).

We'll describe then the very basic points of hard- and software architectures of
 modern computing systems. And in the end of the day we'll present the model project
 based on all the concepts above. Lecturers expect that after the course student
 will be able to login on the HPC-cluster, properly setup the environment, 
compile the source code, run parallel programs on HPC-systems and write scripts for
 data post-processing.

Topic 1. Linux Terminal and SSH session.

Introduction to concepts the Linux is based (file system, process, permissions,
 multi-user mode).

Start with set of standard commands and tips: SSH session, ls ,cd, echo, mkdir,
 rm, pwd, touch, vi(m) organizing for loop, head, tail, sort, and our especial
 friends "|, >, <, &". There will be set of simple exercises on implementation
 of bash-scripts.

Establish remote connection with external Linux machine and find out that you got
 into another linux terminal. Deal with it and study where you are: hostname, 
ifconfig, df , du, exit

Copy data to server and from it. Copy the directory zip-unzip data.

During class 1 students are expected to get through the following practice:

1) Open bash terminal and try first bash commands in interactive mode: 
ls, pwd, vim, tail, head, organization of pipe and IO-redirection
2) Setup ssh connection with remote server and execute particular commands: 
2.1 hostname, df, du, ls, pwd, ifconfig, create and edit file at remote server
2.2 scp data transfer to remote server and from
2.3 organize first bash script with for loop, chmod source into the executable
 mode, process output stream with head and tail

Those who were not in time during the class are welcome to finish it at home.

Topic 2. Environment variables and Makefiles.

Bash profiles, env command, PATH, LD_LIBRARY_PATH, default executable locations,
 static and dynamic libraries.

Compiler gcc, Makefile, linking external libraries, run process in a background.

Further in-class-practice in the same manner as at class 1. Optimistic plan:

1) Copy prepared directory with model program to host server
2) Compile it and provide navigation inside project folders
3) Assign important environment variables and get shortcut to their assignment
 via bashrc
4) Get through reminder about permissions in Linux filesystem
5) Learn sort and hard links
6) Learn commands of packing/unpacking archives at remote server
7) Learn command for download of data from ftp server (from "far away" in 
the internet)
8) Configure your first Makefile and compile external library

Topic 3. Linux High-performance server and to Linux-based cluster.

Establish remote connection to a high-performance computing server.
 Fix skills from Class 2.

Overview of clusters and workload schedulers and their logical setup
 (Torque PBS, SLURM etc).

Schedulers' commands and how to learn them. Practical implementation of the 
simplest SLURM script.

During class 3 practice we expect to 

1) finalize example with organization of first makefile for toy-project
2) login to high-performance computing server and understand why it is different
 from computing cluster
3) learn setup parameters of computing cluster
4) watch examples of usage PBS and SLURM schedulers
5) answer to questions from classes 1 and 2


Topic 4. Going parallel

First class on parallelism.

Implementation of the simplest possible parallel programs (e.g. scalar product)
 within OpenMP and MPI interfaces. Compile, run and post-process data with grep, awk.

During class 4 students are expected to 

1) Compile and run three examples of openMP use: shared-private variables, 
critical section, barrier 
2) Compile and run example of MPI program using gentleman's set of MPI functions
3) Create Makefile for all this stuff
4) Download archive with prepared log-examples and run post-processing bash
 script with awk usage

Topic 5 Going parallel and hello to GPU

1) Utilization and allocation of device, race condition
2) Strong and weak scalability
3) Reminder about memory hierarchy and speedups
4) Shift from CPU-only to GPU
5) CUDA as typical extenion: nvcc
6) Practice: running ready-made benchmarks

Topic 6. Virtual environments

1) Stop: Intel Compiler Set
2) Situations with contradictions between requirements to
environment
3) Virtual Machine as solutions
4) Anaconda as better way for Python/R
5) Containers: Docker, Singularity
