\documentclass{beamer}
\usepackage{amssymb,amsfonts,amsmath,mathtext}
\usepackage{cite,enumerate,float,indentfirst}
\usepackage{tikz}
\usepackage{listings}
\usepackage{pgfplots}
\usepackage{multimedia}

\usetheme{Singapore}

\setbeamercolor{title}{bg=orange,fg=white}
\title{\textbf{Lecture 4. Going parallel }}
\author[shortname]{Sergey Matveev, Mikhail Litsarev}
\date{}

%Class 4. Going parallel
%First class on parallelism. 
%Implementation of the simplest possible parallel programs (scalar product) within OpenMP %and MPI interfaces.
%Compile them, run and postprocess with grep, awk.


\begin{document}

\maketitle

% SLIDE 1
\begin{frame}{Plan for today}
\begin{enumerate}
\item Pre-practice: reminders about process context and threads
\item Multithreading? Easy with OpenMP!
\item Multiple processes? 
\begin{itemize}
\item General logic
\item General words about MPI
\item Hybrid parallel program
\end{itemize}
\item Post-process your output: \textbf{awk}, \textbf{grep}
\item  Practical implementation of mini-project
\end{enumerate}

\end{frame}

\begin{frame}{Expectations during mini-practice}
\begin{itemize}
\item You will ask questions
\item You will ask right questions
\item You will ask Google right questions
\end{itemize}
\end{frame}

\begin{frame}{Amdahl's law}

\begin{equation}
S_{l} = \frac{1}{\alpha + \frac{1-\alpha}{p}}
\end{equation}
\begin{itemize}
\item $S_{l}$ is the theoretical speedup of the execution of the whole task;
\item $\alpha$ is the sequential part of calculations
\item $p$ number of occupied processors
\end{itemize}
\begin{center}
\begin{tabular}{c|c|c|c}
$\alpha$ / p  & 	10 & 	100 &	1 000\\
0 	  & 10   & 	 100   & 	1 000 \\
10 \% & 	5,263      & 	9,174 & 	9,910 \\
25 \% & 	3,077      & 	3,883 & 	3,988 \\
40 \% & 	2,174      & 	2,463 & 	2,496 \\
\end{tabular}
\end{center}
\end{frame}

\begin{frame}{Good and bad algorithms for parallel computing}
Good (usually)
\begin{enumerate}
\item Monte Carlo simulations
\item Finite-difference methods for easy domains
\item Matrix multiplication
\item Many independent simulations :)
\end{enumerate}
Bad (usually)
\begin{enumerate}
\item Time-integration of ODE
\item FFT 
\item Qsort
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Pre-practice}
\textbf{Process and Thread} are different.
Each process has its unique virtual address space and goes through its lifecycle. \\
\textbf{Threads} are just the parts of process and share memory address space but each thread can use CPU itself. \\
\begin{center}
Pluses and minuses of each paradigm?
\begin{enumerate}
\item Scalability
\item Logical setup 
\item Data ``safety''
\item Don't forget about race conditions
\item Overheads
\end{enumerate}
\end{center}
\end{frame}

\begin{frame}{Let's deal with Threads}
Historical part: \textbf{POSIX Threads}. \url{https://en.wikipedia.org/wiki/POSIX_Threads}\\
Not bad but little bit ugly even for easy tasks.\\
\textbf{OpenMP} -- easy and clear interface.
\end{frame}

\begin{frame}{OpenMP}
\begin{center}
\includegraphics[scale=0.3]{IMG/04_Lecture_img1_OpenMP_tree}\\
\url{https://en.wikipedia.org/wiki/OpenMP}
\end{center}
\end{frame}


\begin{frame}[fragile]
\lstset{language=C++,
                basicstyle=\ttfamily,
                keywordstyle=\color{blue}\ttfamily,
                stringstyle=\color{red}\ttfamily,
                commentstyle=\color{green}\ttfamily,
                morecomment=[l][\color{magenta}]{\#}
}
\begin{lstlisting}
#include <stdio.h>
#include <omp.h>
int main(int argc, char ** argv)
{
    int i = 0, j = 0, k = 0, num_threads = 2;
    omp_set_num_threads(num_threads);
    #pragma omp parallel for shared(j) private(k)
    for (i = 0; i < 2 * num_threads; i++)
    {
        j = j + 2 * omp_get_thread_num() + 1;
        k = k + 2 * omp_get_thread_num() + 1;
        printf("Hello %d from thread %d, j = %d\n",
               i, omp_get_thread_num(), j);
    }
    printf("Final Hello , j = %d\n", j);
    printf("Final Hello , k = %d\n", k);
    return 0;
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\lstset{language=C++,
                basicstyle=\ttfamily,
                keywordstyle=\color{blue}\ttfamily,
                stringstyle=\color{red}\ttfamily,
                commentstyle=\color{green}\ttfamily,
                morecomment=[l][\color{magenta}]{\#}
}
\begin{lstlisting}
#include <stdio.h>
#include <omp.h>
int main(int argc, char ** argv){
    int size = 100, j = 0, max = 0, num_thr = 2;
    int a[size];
    //... fulfill array a somehow ...
    omp_set_num_threads(num_thr);
    #pragma omp parallel for shared(max)
    for (i = 0; i < size; i++) {
        if (a[i] > max) {
            #pragma omp critical {
                if (a[i] > max)
                    max = a[i];
            }
        }
    }
    printf("Final max  = %d\n", max);
    return 0;
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\begin{small}
\lstset{language=C++,
                basicstyle=\ttfamily,
                keywordstyle=\color{blue}\ttfamily,
                stringstyle=\color{red}\ttfamily,
                commentstyle=\color{green}\ttfamily,
                morecomment=[l][\color{magenta}]{\#}
}
\begin{lstlisting}
int main( )   
{  
    int a[5], i;  
    #pragma omp parallel  
    {  
        // Perform some computation.  
        #pragma omp for  
        for (i = 0; i < 5; i++)  
            a[i] = i * i;   
        #pragma omp master  
            for (i = 0; i < 5; i++)  
                printf("a[%d] = %d\n", i, a[i]);  
        // Wait.  
        #pragma omp barrier  
        // Continue with the computation.  
        #pragma omp for  
        for (i = 0; i < 5; i++)  
            a[i] += i;  
    }  
}  
\end{lstlisting}
\end{small}
\end{frame}

\begin{frame}{What's Beyond openMP? MPI!}
\textbf{MPI} (Message Passing Interface) is useful if you want adopt your parallel program for distributed computations (e.g. at cluster).\\
MPI-application will ``generate'' full-weight processes. \\
Each process will have \textbf{own virtal memory address space} and context. But they will be abble to communicate via network with use of this guy. 
\begin{center}
\textbf{Discussions? Pluses and minuses?}
\end{center}
\end{frame}

\begin{frame}{MPI basic structures}
Each process gets its unique ``rank''-number. Processes are grouped within ``communicators''.\\
Interactions are performed within ``receive-process-send'' operations
\begin{itemize}
\item Communicator
\item Point-to-point interation
\item Collective operations
\item Deriviation of structures for messages
\end{itemize}
\end{frame}

\begin{tiny}
\lstset{language=C++,
                basicstyle=\ttfamily,
                keywordstyle=\color{blue}\ttfamily,
                stringstyle=\color{red}\ttfamily,
                commentstyle=\color{green}\ttfamily,
                morecomment=[l][\color{magenta}]{\#}
}
\begin{lstlisting}
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>

int main(int argc, char **argv)
{
    char buf[256];
    int my_rank, num_procs;
    /* Initialize the infrastructure necessary for communication */
    MPI_Init(&argc, &argv);
    /* Identify this process */
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    /* Find out how many total processes are active */
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    /* Until this point, all processes have been doing exactly the same.
       Here, we check the rank to distinguish the roles */
    if (my_rank == 0) {
        int other_rank;
        printf("We have %i processes.\n", num_procs);
        /* Send messages to all other processes */
        for (other_rank = 1; other_rank < num_procs; other_rank++)
        {
            sprintf(buf, "Hello %i!", other_rank);
            MPI_Send(buf, sizeof(buf), MPI_CHAR, other_rank,
                     0, MPI_COMM_WORLD);
        }
        /* Receive messages from all other process */
        for (other_rank = 1; other_rank < num_procs; other_rank++)
        {
            MPI_Recv(buf, sizeof(buf), MPI_CHAR, other_rank,
                     0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            printf("%s\n", buf);
        }
    }
     
    else {
        /* Receive message from process #0 */
        MPI_Recv(buf, sizeof(buf), MPI_CHAR, 0,
                 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        assert(memcmp(buf, "Hello ", 6) == 0),
        /* Send message to process #0 */
        sprintf(buf, "Process %i reporting for duty.", my_rank);
        MPI_Send(buf, sizeof(buf), MPI_CHAR, 0,
                 0, MPI_COMM_WORLD);
    }
    /* Tear down the communication infrastructure */
    MPI_Finalize();
    return 0;
}
\end{lstlisting}
\end{tiny}

\begin{frame}{Let's compile and run it}

\begin{itemize}
\item \texttt{mpicc.openmpi MPI\_example.c -o MPI\_example.out}
\item \texttt{mpiexec.openmpi -np 4 ./MPI\_example.out}
\end{itemize}

\end{frame}

\begin{frame}{Gentleman's set?}
\begin{itemize}
\item \textbf{MPI\_Init(\&argc, \&argv)} -- get through initialization
\item \textbf{MPI\_Comm\_rank(MPI\_COMM\_WORLD, \&my\_rank)} -- who am I?
\item \textbf{MPI\_Comm\_size(MPI\_COMM\_WORLD, \&num\_procs)} -- where am I?
\item \textbf{MPI\_Send(buf, sizeof(buf), MPI\_CHAR, other\_rank,
                     0, MPI\_COMM\_WORLD)} -- send some ``post-card''
\item \textbf{MPI\_Recv(buf, sizeof(buf), MPI\_CHAR, other\_rank,
                     0, MPI\_COMM\_WORLD, MPI\_STATUS\_IGNORE)} -- get some ``post-card''
\item \textbf{MPI\_Finalize()} -- shutdown interface.
\end{itemize}
\end{frame}

\begin{frame}{Post-processing}
\begin{center}
\textbf{Let's grep it!}\footnote{grep searches the input files for lines containing a match to a given pattern list. }
\end{center}

\begin{itemize}
\item grep root /etc/passwd
\item grep -nH root /etc/passwd
\item grep -v bash /etc/passwd | grep -v nologin
\item Let's go to \\
\url{http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_04_02.html} \\
and continue...
\end{itemize}

\end{frame}

\begin{frame}{Post-processing}
\begin{center}
Okay, and what if I want to get particular column from file? 
Let's \textbf{awk} it!\footnote{AWK is a programming language designed for text processing and typically used as a data extraction and reporting tool. It is a standard feature of most Unix-like operating systems.}\\
\textbf{awk   '{print \$1" "\$2}' datafile } -- will extract column 1 and column 2 from your data file. Space it default separator.\\
\textbf{awk -F'\#' '{print \$1" "\$2}' datafile} -- will make \# separator symbol.\\
More examples at \\ \url{https://www.tutorialspoint.com/awk/awk_basic_examples.htm}
\end{center}
\end{frame}


\begin{frame}
\begin{center}
\textbf{Practice!}
\end{center}
\end{frame}

\begin{frame}{bash + awk example for data processing}
\begin{footnotesize}
\textbf{wget http://10.30.99.214/smatveev-www/EXAMPLE-processing.tar}\\
and extract the data
\textbf{tar -xzvf EXAMPLE\_processing.tar}\\
\textbf{ls} -- must indicate maaaany files!
let's search for 2 of them using grep and ...
\end{footnotesize}

\end{frame}

\begin{frame}{paste}
Using paste for combining the files:\\

\textit{paste file1 file2}


\end{frame}

\begin{frame}[fragile]{wait for background}
\begin{lstlisting}[language=bash]
#!/bin/bash

sleep 10 &
echo hello
sleep 43 &
\end{lstlisting}
Compare with
\begin{lstlisting}[language=bash]
#!/bin/bash

sleep 10 &
echo hello
sleep 43 &
wait
\end{lstlisting}

\end{frame}

\begin{frame}{Computer lab / project topics}

\begin{enumerate}
\item Chose your own topic but you have not more than 1 week to fix the topic
\item Performance of Matmul with use of OpenMP or (and) MPI or (and) Cuda
\item Performance of FFT with use of OpenMP or (and) MPI or (and) Cuda
\item Take ready-made benchmark from github and present it
\item Compare the compilers (Intel vs GNU) and optimization options (e.g. -O3)
\end{enumerate}

\end{frame}

\end{document}


