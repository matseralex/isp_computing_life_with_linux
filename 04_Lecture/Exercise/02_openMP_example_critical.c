#include <stdio.h>
#include <omp.h>
int main(int argc, char ** argv){
    int size = 100, j = 0, max = 0, num_thr = 2;
    int a[size];
    int i;
    //... fulfill array a somehow ...
    omp_set_num_threads(num_thr);
    #pragma omp parallel for shared(max)
    for (i = 0; i < size; i++) {
        if (a[i] > max) {
            #pragma omp critical 
            {
                if (a[i] > max)
                    max = a[i];
            }
        }
    }
    printf("Final max  = %d\n", max);
    return 0;
}
