#include <stdio.h>
#include <omp.h>
int main( )   
{  
    int a[5], i;  
    #pragma omp parallel  
    {  
        // Perform some computation.  
        #pragma omp for  
        for (i = 0; i < 5; i++)  
            a[i] = i * i;   
        #pragma omp master  
            for (i = 0; i < 5; i++)  
                printf("a[%d] = %d\n", i, a[i]);  
        // Wait.  
        #pragma omp barrier  
        // Continue with the computation.  
        #pragma omp for  
        for (i = 0; i < 5; i++)  
            a[i] += i;  
    }  
}  
