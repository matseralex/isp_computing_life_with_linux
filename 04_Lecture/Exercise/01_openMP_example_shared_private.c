#include <stdio.h>
#include <omp.h>
int main(int argc, char ** argv)
{
    int i = 0, j = 0, k = 0, num_threads = 2;
    omp_set_num_threads(num_threads);
    #pragma omp parallel for shared(j) private(k)
    for (i = 0; i < 2 * num_threads; i++)
    {
        j = j + 2 * omp_get_thread_num() + 1;
        k = k + 2 * omp_get_thread_num() + 1;
        printf("Hello %d from thread %d, j = %d\n",
               i, omp_get_thread_num(), j);
    }
    printf("Final Hello , j = %d\n", j);
    printf("Final Hello , k = %d\n", k);
    return 0;
}
