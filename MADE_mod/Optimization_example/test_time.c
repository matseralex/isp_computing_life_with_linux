#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

int main()
{
    int N = 2048;
    struct timeval start, end;
    double r_time = 0.0;
    int i, j, k;
    gettimeofday(&start, NULL);
    double *A, *B, *C;
    A = (double *) malloc(N * N * sizeof(double));
    B = (double *) malloc(N * N * sizeof(double));
    C = (double *) malloc(N * N * sizeof(double));
    for (i = 0; i < N; i++)
        for(j = 0; j < N; j++)
        {
            C[i * N + j] = 0.0;
            for(k = 0; k < N; k++)
                C[i * N + j] = C[i * N + j] + A[i * N + k] * B[k * N + j];
        }
    gettimeofday(&end, NULL);
    r_time = end.tv_sec - start.tv_sec + ((double) (end.tv_usec - start.tv_usec)) / 1000000;
    printf("%lf runtime, sec\n", r_time);
    free(A); 
    free(B);
    free(C);
    return 0;
}
