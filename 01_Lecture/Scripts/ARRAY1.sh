#!/bin/bash
my_array=(apple banana "Fruit Basket" orange)
my_array[2]=apricot

my_array=(apple banana "Fruit Basket" orange)
echo "number of elements =" ${#my_array[@]}
echo "element 3 " ${my_array[2]}    # element
my_array[4]="carrot"
echo "number of elements =" ${#my_array[@]}  
echo "element 5 " ${my_array[4]}    # element

my_array[2]="carrot"
echo "number of elements =" ${#my_array[@]}
echo "element 3 " ${my_array[2]}    # element

