#!/bin/bash

NAME="John"
if [ "$NAME" = "John" ]; then
  echo "True - my name is indeed John"
fi

NAME="George"
if [ "$NAME" = "John" ]; then
  echo "John Lennon"
elif [ "$NAME" = "George" ]; then
  echo "George Harrison"
else
  echo "This leaves us with Paul and Ringo"
fi

