#!/bin/bash

A=1
echo $A
my_array=(apple banana "Fruit Basket" orange)
echo ${my_array[3]}
# orange - note that curly brackets are needed
# adding another array element
my_array[4]="carrot"
# value assignment without a $ and curly brackets
echo ${#my_array[@]}
# 5
echo ${my_array[${#my_array[@]}-1]}
# carrot
my_array[0]="bad apple"
echo ${my_array[0]}
