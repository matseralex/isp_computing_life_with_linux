#!/bin/bash
if [ "$1" = "opt1" ]; then
	gcc -c -fpic foo.c
	gcc -c -fpic foo2.c
	gcc -shared -o libfoo.so foo.o foo2.o
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${PWD}
	gcc main.c -L./ -lfoo  -o test 
	./test
elif [ "$1" = "opt2" ]; then
	gcc -c -fpic foo.c
	gcc -c -fpic foo2.c
	gcc -shared -o libfoo.so foo.o foo2.o
	gcc -L./ -Wl,-rpath=./ -o test main.c -lfoo
	./test
fi
