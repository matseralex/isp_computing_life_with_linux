#!/usr/bin/env python3
import sys
import time
from numba import njit
import numpy as np, numba as nb
from time import perf_counter as clock

@njit(nb.int64(nb.uint8[::1], nb.uint8[::1]))
def lev_dist(s1, s2):
    m = len(s1)
    n = len(s2)

    # Edge cases.
    if m == 0: return n
    elif n == 0: return m

    v0 = np.arange(n + 1)
    v1 = v0.copy()

    for i, c1 in enumerate(s1):
        v1[0] = i + 1

        for j, c2 in enumerate(s2):
            subst_cost = v0[j] if c1 == c2 else (v0[j] + 1)
            del_cost = v0[j + 1] + 1
            ins_cost = v1[j] + 1

            min_cost = min(subst_cost, del_cost, ins_cost)
            v1[j + 1] = min_cost

        v0, v1 = v1, v0

    return v0[n]

if __name__ == "__main__":

    fout = open('py.txt', 'w')    
    for n in 1000, 2000, 5000, 10000, 15000, 20000, 25000:    
        s1 = np.full(n, ord('a'), dtype=np.uint8)
        s2 = s1
        s3 = np.full(n, ord('b'), dtype=np.uint8)

        exec_time = -clock()

        print(lev_dist(s1, s2))
        print(lev_dist(s1, s3))

        exec_time += clock()
        print(f'{n} {exec_time:.8f}', file=fout)
