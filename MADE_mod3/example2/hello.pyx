from libc.math cimport pow

cpdef double square_and_add (double x):
    """Compute x^2 + x as double.

    This is a cdef function that can be called from within
    a Cython program, but not from Python.
    """
    a = 29669093332083606603617799242426306347429462625218523944018571574194370194723262390744910112571804274494074452751891
    b = 34038161751975634380066094984915214205471217607347231727351634132760507061748526506443144325148088881115083863017669
    print(a * b)
    return pow(x, 2.0) + x

cpdef print_result (double x):
    """This is a cpdef function that can be called from Python."""
    print("({} ^ 2) + {} = {}".format(x, x, square_and_add(x)))
