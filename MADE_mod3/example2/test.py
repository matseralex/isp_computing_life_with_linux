# Import the extension module hello.
import hello
import time
import computepi
# Call the print_result method 
hello.print_result(23.0) 
n = 10000000
start_time = time.time()
Pi=computepi.compute_pi(n)
print("Cython call 1 --- %s seconds ---" % (time.time() - start_time))

start_time = time.time()
Pi=computepi.compute_pi(n)
print("Cython call 2 --- %s seconds ---" % (time.time() - start_time))
