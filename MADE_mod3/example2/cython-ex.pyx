import random
# cython_module.pyx              WRITE THE BINDINGS
cpdef double compute_pi (int nsamples):
    """
    compute pi
    parameter: nsamples -- integer
    """
    cdef int i
    cdef double x, y
    cdef int acc
#    srand(time(NULL))
#    for (i = 0; i < nsamples; i++)
    for i in range(nsamples):
        x = random.random()
        y = random.random()
        if (x **2 + y**2 <= 1):
            acc = acc + 1
    return 4.0 * acc / nsamples 
