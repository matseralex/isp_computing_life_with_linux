from SALib.sample import saltelli
from SALib.analyze import sobol
from SALib.test_functions import Ishigami
from math import *
import numpy as np
import time
from numba import njit 

a = 0.1
b = 0.3

@njit
def evaluate_model(x):
    s = 0
    for i in range(100):
        s += i* x[i % 2]
    return sin(x[0]) + a * sin(x[1])*sin(x[1]) + b * sin(x[0]) * x[2]**3 + s

problem = {
    'num_vars': 3,
    'names': ['x1', 'x2', 'x3'],
    'bounds': [[-3.14159265359, 3.14159265359],
               [-3.14159265359, 3.14159265359],
               [-3.14159265359, 3.14159265359]]
}
n = 100000
start = time.time()
param_values = saltelli.sample(problem, n)
print("samples generation took %s seconds" %(time.time() - start))
print(param_values)
Y = np.zeros([param_values.shape[0]])

start = time.time()
for i, X in enumerate(param_values):
    Y[i] = evaluate_model(X)

print("model evaluation took %s seconds" %(time.time() - start))
#print(Y)

start = time.time()
Si = sobol.analyze(problem, Y)

print("SA took %s seconds" %(time.time() - start))
# first-order indices
print(Si['S1'])
# total indices for each variable
print(Si['ST'])
# second order indices
print("x1-x2:", Si['S2'][0,1])
print("x1-x3:", Si['S2'][0,2])
print("x2-x3:", Si['S2'][1,2])



