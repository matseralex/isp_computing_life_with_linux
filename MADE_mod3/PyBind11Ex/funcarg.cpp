#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <omp.h>
namespace py = pybind11;

std::function<double (const int, const int)> func_arg(const std::function<double(const int,const int)> &f)
{
    return [f](int i, int j)
    {
        return 1.0 + f(i, j);
    };
}

double call_standard_python(const int N)
{
     py::print("Helloworld");
     double sum = 0.0; 
     // omp parallel for works fine
     #pragma omp parallel for reduction(+:sum)
     for (int i = 0; i < N; i++)
         sum = sum + i;
     py::print(sum);
     return sum;
}


double operate_with_f(const std::function<double(const int,const int)> &f, int N)
{
     double sum = 0.0; 
     // omp parallel for make program freeze
     //#pragma omp parallel for reduction(+:sum)
     for (int i = 0; i < N; i++)
         sum = sum + f(i,i);
     printf("hello\n");
     return sum;
}


PYBIND11_MODULE(funcarg, m) {
    m.doc() = "pybind11 example plugin"; // optional module docstring
    m.def("func_arg", &func_arg, "Example for functional argument");
    m.def("call_standard_python", &call_standard_python, "Example for calling Pyton");
    m.def("operate_with_f", &operate_with_f, "Example for calling outer");
}
