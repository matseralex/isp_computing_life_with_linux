#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
//#include <cblas.h>
#include <omp.h>
namespace py = pybind11;

py::array_t<double> daxpy_mod_ex(py::array_t<double> input1, py::array_t<double> input2, double alpha)
{
// return z = alpha * x + y
    py::buffer_info buf1 = input1.request();
    py::buffer_info buf2 = input2.request();
    if (buf1.ndim != 1 || buf2.ndim != 1)
        throw std::runtime_error("Number of dimensions must be one");

    if (buf1.size != buf2.size)
        throw std::runtime_error("Input shapes must match");

    /* No pointer is passed, so NumPy will allocate the buffer */
    auto result = py::array_t<double>(buf1.size);

    py::buffer_info buf3 = result.request();

    double *ptr1 = (double *) buf1.ptr,
           *ptr2 = (double *) buf2.ptr,
           *ptr3 = (double *) buf3.ptr;
    int N = buf1.shape[0];
    
    #pragma omp parallel for
    for (int idx = 0; idx < N; idx++)
        ptr3[idx] = alpha * ptr1[idx] + ptr2[idx];

    //for (int idx = 0; idx < N; idx++)
    //    ptr3[idx] = ptr2[idx];
    //cblas_daxpy(N, alpha, ptr1, 1, ptr3, 1);
    return result;
}


PYBIND11_MODULE(exdaxpy, m) {
    m.doc() = "pybind11 example plugin"; // optional module docstring
    m.def("daxpy_mod_ex", &daxpy_mod_ex, "Return z = alpha * x + y");
}
