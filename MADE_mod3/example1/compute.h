
double compute_pi(int nsamples);
double compute_pi_omp(int nsamples);
double par_for(int nsamples);
