#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "compute.h"
#include <omp.h>
double compute_pi(int nsamples)
{
    int i; 
    int acc = 0;
    srandom(time(NULL));
    double x, y, z;
    for (i = 0; i < nsamples; i++)
    {
      x = (double)rand() / RAND_MAX;
      y = (double)rand() / RAND_MAX;
      z = x * x + y * y;
      if (z <= 1.0)
          acc++;
    }
    return (4.0 * ((double) acc)) / nsamples;
}

double compute_pi_omp(int nsamples)
{
    double x, y, z;
    int count = 0;       
    double pi;          
    int numthreads = 0;
    int threadname, chunk;
    int *seeds;
    srand(time(NULL));

    #pragma omp parallel shared(numthreads,chunk,seeds)
    {
        numthreads = omp_get_num_threads();
        chunk = nsamples / numthreads;
    }
    seeds = calloc(numthreads, sizeof(int));
    srand(time(NULL));
    #pragma omp parallel shared(chunk, numthreads, seeds) private(x, y, z, threadname) reduction(+:count)
    {
        threadname = omp_get_thread_num();
        seeds[threadname] = rand();
        for (int i = 0; i < chunk; i++)
        {
            x = (double)rand_r(seeds + threadname) / RAND_MAX;
            y = (double)rand_r(seeds + threadname) / RAND_MAX;
            z = x * x + y * y;
            if (z <= 1.0)
            {
                count+= 1;
            }
        }
    } 
    free(seeds);
    pi = ((double)count / (double)(nsamples)) * 4.0;
    printf("Pi: %f\n", pi); 
    return pi;
}


double par_for(int nsamples)
{
    double x = 0.0;
    #pragma omp parallel for reduction(+:x)
    for (int i = 0; i < nsamples; i++)
    {
        if (i % 2 == 0)
            x += i * 0.5;
    }
    return x;
}
