import time
from numba import jit
import random
import ctypes
import os
clibPI = ctypes.CDLL('./libPI.so')

@jit(nopython=True,parallel=True,nogil=True)
def jit_monte_carlo_pi_for(nsamples):
    acc = 0
    for i in range(nsamples):
        x = random.random()
        y = random.random()
        if (x ** 2 + y ** 2) < 1.0:
            acc += 1
    return 4.0 * acc / nsamples

def monte_carlo_pi_for(nsamples):
    acc = 0
    for i in range(nsamples):
        x = random.random()
        y = random.random()
        if (x ** 2 + y ** 2) < 1.0:
            acc += 1
    return 4.0 * acc / nsamples

def monte_carlo_pi_while(nsamples):
    acc = 0
    i = 0 
    while i < nsamples:
        x = random.random()
        y = random.random()
        if (x ** 2 + y ** 2) < 1.0:
            acc += 1
        i += 1
    return 4.0 * acc / nsamples

@jit(nopython=True)
def jit_monte_carlo_pi_while(nsamples):
    acc = 0
    i = 0 
    while i < nsamples:
        x = random.random()
        y = random.random()
        if (x ** 2 + y ** 2) < 1.0:
            acc += 1
        i += 1
    return 4.0 * acc / nsamples

n = 10000000

print("Compute Pi with %d samples" %n )

start_time = time.time()
monte_carlo_pi_for(n)
print("naive call for 1 --- %s seconds ---" % (time.time() - start_time))
start_time = time.time()
monte_carlo_pi_for(n)
print("naive call for 2 --- %s seconds ---" % (time.time() - start_time))

start_time = time.time()
jit_monte_carlo_pi_for(n)
print("jit call for 1 --- %s seconds ---" % (time.time() - start_time))
start_time = time.time()
jit_monte_carlo_pi_for(n)
print("jit call for 2 --- %s seconds ---" % (time.time() - start_time))


start_time = time.time()
monte_carlo_pi_while(n)
print("naive call while 1 --- %s seconds ---" % (time.time() - start_time))
start_time = time.time()
monte_carlo_pi_while(n)
print("naive call while 2 --- %s seconds ---" % (time.time() - start_time))


start_time = time.time()
jit_monte_carlo_pi_while(n)
print("jit call while 1 --- %s seconds ---" % (time.time() - start_time))
start_time = time.time()
jit_monte_carlo_pi_while(n)
print("jit call while 2 --- %s seconds ---" % (time.time() - start_time))

start_time = time.time()
os.system("./a.out")
print("os system 1 --- %s seconds ---" % (time.time() - start_time))

start_time = time.time()
os.system("./a.out")
print("os system 2 --- %s seconds ---" % (time.time() - start_time))

clibPI.compute_pi.restype = ctypes.c_double
start_time = time.time()
answer = clibPI.compute_pi(ctypes.c_int(n))
print("call C --- %s seconds ---" % (time.time() - start_time))
print("answer: myPi %lf" %answer)
