#include <stdio.h>
#include"compute.h"
#include <time.h>
#include<omp.h>
int main()
{
    int n = 10000000;
    clock_t start, end;
    start = clock();
    start = omp_get_wtime();
    double pi = compute_pi(n);
    end = clock();
    printf("result %lf\n time %lf sec\n", pi, (double )(end - start)/ CLOCKS_PER_SEC);

    double ompstart, ompend;
    ompstart = omp_get_wtime();
    pi = compute_pi_omp(n);
    ompend = omp_get_wtime();
    printf("omp result %lf\n time %lf sec\n", pi, (ompend - ompstart));


    return 0;

}


